$(document).ready(function(){
    PopUpHide();
});

function PopUpShow(){
    $("#popup1").show();
    history.pushState(null,'commform','./communicatefrom');
    $("#email").val(localStorage.getItem('email'));
    $("#name").val(localStorage.getItem('name'));
    $("#phone").val(localStorage.getItem('phone'));
    $("#mess").val(localStorage.getItem('mess'));

}

function PopUpHide(){
    $("#popup1").hide();
    history.replaceState({id: null}, 'Default state', './');
}



window.addEventListener('popstate', function() {
        PopUpHide();
});
